# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 6.2.3 2024-08-12
### Fixed
- Wait-PdqDeployment would exit without ever waiting on some deployments because their status was briefly `Queued`.
  - [Issue #118](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/118)

## 6.2.2 2024-07-24
### Fixed
- Get-PdqLatestVersion recently started throwing an error when using version numbers higher than the current latest version.
  - [Issue #117](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/117)

## 6.2.1 2024-03-25
### Fixed
- SimplySql 2.0 breaks Invoke-PdqSqlQuery
  - [Issue #111](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/111)

## 6.2.0 2023-08-19
### Changed
- Refactored Connect-PdqInventoryScanner.
  - [Issue #101](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/101)
  - Rewrote all prompts.
  - Added support for selecting multiple Scanners at once (from the same Scan Profile).
  - Added support for selecting Scan Profiles. All of their Scanners will be selected.
  - The selected Scanners are displayed.
  - Added a filter to prevent non-configurable Scanners from being selected.

## 6.1.1 2023-05-24
### Fixed
- Start-PdqDeployment: Targets should be separated by a space, not a comma.
  - [Issue #99](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/99)

## 6.1.0 2023-05-24
### Added
- Wait-PdqDeployment
  - [Issue #97](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/97)
- Start-PdqDeployment
  - [Issue #95](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/95)

## 6.0.2 2023-05-24
### Changed
- Filter IDs in Get-PdqDeployment's SQL query to improve efficiency.
  - [Issue #98](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/98)

## 6.0.1 2023-05-23
### Fixed
- Convert-PdqDocumentationLink now handles links that contain `index.html`.
  - [Issue #96](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/96)

## 6.0.0 2022-11-06
### Changed
- Create an object for Set-PdqCustomVariable's output
  - [Issue #86](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/86)
  - This is a potentially breaking change, so I bumped the major version

### Added
- Sync-PdqVariable
  - [Issue #24](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/24)

## 5.3.0 2022-11-02
### Added
- Get, Set, and Remove -PdqStuffSetting
  - [Issue #89](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/89)

## 5.2.6 2022-11-02
### Fixed
- Set-PdqCustomVariable now detects and returns any errors the PDQ CLI returns
  - [Issue #87](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/87)

## 5.2.5 2022-11-02
### Fixed
- Move-PdqDeployItem no longer allows you to select multiple folders as the destination
  - [Issue #88](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/88)

## 5.2.4 2022-11-02
### Fixed
- Move-PdqDeployItem no longer allows you to move a folder into its own child
  - [Issue #65](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/65)

## 5.2.3 2022-10-29
### Changed
- Use Write-Error instead of throw in Get-PdqVariable if a variable cannot be found
  - [Issue #81](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/81)
  - Deploy mishandles terminating errors, and this error does not need to be terminating

## 5.2.2 2022-10-27
### Changed
- Updated the Description for this module

## 5.2.1 2022-09-23
### Fixed
- Variables that are updated with Set-PdqCustomVariable will now properly update collections
  - [Issue #82](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/82)
  - Unfortunately, I had to switch to using the PDQ CLI EXEs. It's nowhere near as clean as accessing the database directly...but it actually works.
    - This has the side effect of requiring your session to be running as admin

## 5.2.0 2022-06-12
### Added
- Setting to disable warning when importing this module
  - See [Settings](README.md#settings) for more details

## 5.1.1 2022-04-07
### Changed
- Convert Get-PdqLatestVersion's buildDate property to local time, and use a simpler format
  - [Issue #73](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/73)

## 5.1.0 2022-04-02
### Added
- New functions
  - Invoke-PdqPkgDownload
    - [Issue #72](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/72)
    - Helper functions:
      - Get-PdqPackageLibraryCatalog
      - Select-PdqPackageLibraryPackage
- 2 options to the -Format parameter of Get-PdqLicense
  - URL and Wrapped
- -AllowFreeMode parameter to Get-PdqLicense

### Fixed
- Get-PdqLicense will now throw the correct error if no license can be found in the registry

## 5.0.3 2022-03-20
### Changed
- PdqStuff.psd1's encoding from UTF-16 LE to UTF-8
  - [Issue #70](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/70)

## 5.0.2 2022-03-20
### Fixed
- Hide-PdqNonDownloadedPackageUpdates's second Example was incorrectly formatted
  - [Issue #68](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/68)

## 5.0.1 2022-03-20
### Fixed
- Wait-PdqClipboardData was listed as Wait-PdqClipboard in PdqStuff.psd1
  - [Issue #67](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/67)

## 5.0.0 2022-03-20
### Changed
- Renamed Move-PdqDeployPackage to Move-PdqDeployItem
  - [Issue #64](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/64)
  - Can now move multiple items at once.
  - Added support for Folders and Target Lists.

## 4.4.0 2022-02-15
### Added
- New function
  - Enable-PdqPackageAlwaysPrioritized
    - [Issue #63](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/63)

## 4.3.0 2022-01-23
### Added
- New function
  - Get-PdqLicense
    - [Issue #62](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/62)

## 4.2.0 2021-06-29
### Added
- New functions
  - Get-PdqLatestVersion
    - [Issue #11](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/11)
  - Get-PdqConsoleUserSession

## 4.1.0 2021-06-29
### Added
- ComputerId support to Get-PdqInventoryComputerData and Get-PdqCustomField.
  - [Issue #49](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/49)

### Fixed
- Get-PdqInventoryComputerData
  - Error with `-Table 'Computers'`.
    - [Issue #51](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/51)
  - Error with tables that start with ~.
    - [Issue #52](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/52)

## 4.0.0 2021-06-27
### Added
- New functions
  - Get-PdqDatabaseData
    - [Issue #33](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/33)
  - Get-PdqCustomField
    - [Issue #32](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/32)

### Changed
- Get-PdqInventoryComputerData
  - -Table now accepts an array of table names.
    - All tables will be joined with an INNER JOIN.
    - [Issue #44](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/44)
  - If -Columns isn't specified, all of the table's columns will be looked up and used.
    - [Issue #45](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/45)
  - The output type is now PSCustomObject instead of DataRow.
    - This is potentially a breaking change, so I bumped the major version.
    - [Issue #46](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/46)
- *-PdqSqlConnection
  - Both functions now log which function called them.
    - Open- logs whether it will open or reuse a connection.
    - Close- logs whether it will close or ignore a connection.
    - [Issue #47](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/47)
- Get-PdqDatabaseTable
  - Completely refactored to use a query that returns both tables and columns, instead of having to look up the columns for each table separately.
    - [Issue #48](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/48)

## 3.0.0 2021-06-26
### Added
- SimplySql parameters (-AsDataTable, -Parameters, and -Stream) to Invoke-PdqSqlQuery.
- Type and Columns properties to the output of Get-PdqDatabaseTable.
- -AsHashtable parameter to Get-PdqDatabaseTable.
- Some sanity checks to Get-PdqInventoryComputerData.
- Tab-completion to the -Table and -Columns parameters of Get-PdqInventoryComputerData.
  - They will now dynamically populate all possible values when you hit TAB or CTRL+SPACE.
  - [Issue #35](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/35)

### Changed
- Assert-PdqMinimumVersion to retrieve the version number from the database instead of the registry.
  - [Issue #6](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/6)
- Order of parameters in Invoke-PdqSqlQuery.
- The following functions to return all data if their filter parameter (-Format, -Id, -Name) isn't specified, accept an array in their filter parameter, and throw an error if any of the values specified in their filter parameter can't be found.
  - [Issue #38](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/38)
  - Resolve-PdqClipboardFormat
    - Changed the name back to Get-PdqClipboardFormat.
    - Changed the output type from Hashtable to PSCustomObject.
  - Get-PdqDatabaseTables
    - Changed the name to Get-PdqDatabaseTable.
    - Added the -Name parameter.
  - Get-PdqDeployments
    - Changed the name to Get-PdqDeployment.
    - Added the -Id parameter.
  - Get-PdqVariable
    - Now outputs many properties in a PSCustomObject instead of just the Value.
  - Get-PdqInventoryComputerData
    - Added ComputerName and HostName to the output to distinguish rows from multiple computers.
    - [Issue #42](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/42)
- Wait-PdqClipboard to Wait-PdqClipboardData.
  - [Issue #39](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/39)

### Fixed
- Hide-PdqNonDownloadedPackageUpdates so it only updates rows if IsUpdated = 1.
  - [Issue #31](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/31)
- Many parameters that I thought were mandatory, but weren't (because they had `[ValidateSet()]`).
  - [Issue #40](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/40)

### Removed
- Get-PdqDatabaseColumns because Get-PdqDatabaseTable now includes the Columns property.

## 2.1.0 - 2021-06-23
### Added
- New function
  - Get-PdqDeployments
    - [Issue #30](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/30)

## 2.0.0 - 2021-06-23
### Added
- New functions
  - Get-PdqVariable
    - [Issue #25](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/25)
  - Set-PdqCustomVariable
    - [Issue #27](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/27)
  - Convert-PdqVariableType
  - Test-PdqVariableName

### Changed
- Renamed functions (this is a breaking change, so I bumped the major version.)
  - Get-PdqClipboardFormat --> Resolve-PdqClipboardFormat
    - [Issue #28](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/28)
  - Get-PdqInventoryComputerId --> Resolve-PdqInventoryComputerName
    - [Issue #29](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/29)

## 1.3.0 - 2021-06-21
### Added
- New functions
  - Compare-PdqInventoryComputers
    - [Issue #16](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/16)
  - Close-PdqSqlConnection
  - Open-PdqSqlConnection
  - Get-PdqDatabaseColumns
  - Get-PdqDatabaseTables
  - Get-PdqInventoryComputerData
  - Get-PdqInventoryComputerId
  - Measure-PdqDeploymentStartDelay
- `[CmdletBinding()]` to all functions.
  - I really should have had it on all of them to begin with.

### Changed
- Get-PdqDatabasePath to verify that the database it found is for the right product.
  - The Database Access columns in the Functions section of the README were updated to reflect this.
- All functions that open a SQLite connection to use Open-PdqSqlConnection and Close-PdqSqlConnection.
  - Get-PdqDatabasePath is excluded from this to avoid a cyclic dependency.

### Removed
- `Default:` from parameter help comments.
  - [Issue #17](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/17)

## [1.2.1] - 2021-06-01
### Fixed
- Hide-PdqNonDownloadedPackageUpdates
  - The trigger caused an error in Deploy when new packages were added to the Package Library. [#1](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/1)
    - Run `Hide-PdqNonDownloadedPackageUpdates` after updating this module to 1.2.1 or later to get the new trigger.
  - New packages bypassed the trigger. A new trigger was added to catch these. [#2](https://gitlab.com/ColbyBouma/pdqstuff/-/issues/2)
    - If you want new packages to appear in the Updates tab, use the new `-AllowNewPackages` parameter to block the creation of the new trigger.
  - A verbose message was never firing.

### Added
- 'Filter' to the list of formats in Get-PdqClipboardFormat

## [1.2.0] - 2021-05-19
### Fixed
- A typo in Get-PdqInstalledVersion's `.EXAMPLE` section.

### Added
- Invoke-PdqSqlQuery.
- All function names to the manifest.
- This changelog.

## [1.1.1] - 2021-05-18
### Fixed
- Get-PdqDatabasePath now allows `-DatabasePath` to be null.
  - This was breaking several functions like Get-PdqInventoryScanErrors since they default to passing a null value.
- Hide-PdqNonDownloadedPackageUpdates was incorrectly throwing an `Unable to create trigger.` error.
- Hide-PdqNonDownloadedPackageUpdates would close your session if you used `-RemoveTrigger`.

### Added
- `[CmdletBinding()]` to Hide-PdqNonDownloadedPackageUpdates.

## [1.1.0] - 2021-05-18
### Fixed
- Set Get-PdqInstalledVersion's `.OUTPUT` section to the correct type.
- Refactored Get-PdqInstalledVersion to search the registry instead of using the PDQ CLI.
  - Now it now longer requires your session to be running as admin.

### Added
- `[CmdletBinding()]` to Get-PdqInstalledVersion.

## [1.0.0] - 2021-05-18
- Initial release.

[1.2.1]: https://gitlab.com/ColbyBouma/pdqstuff/-/compare/1.2.0...1.2.1
[1.2.0]: https://gitlab.com/ColbyBouma/pdqstuff/-/compare/1.1.1...1.2.0
[1.1.1]: https://gitlab.com/ColbyBouma/pdqstuff/-/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/ColbyBouma/pdqstuff/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/ColbyBouma/pdqstuff/-/commits/1.0.0