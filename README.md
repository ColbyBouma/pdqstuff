# PdqStuff

This is a [PowerShell module](https://www.powershellgallery.com/packages/PdqStuff) full of fun little functions I wrote for working with [PDQ Deploy](https://www.pdq.com/pdq-deploy/) and [Inventory](https://www.pdq.com/pdq-inventory/).

Many of these functions access your PDQ databases, so please back them up before proceeding.

# Requirements

- PowerShell version
  - 5.1
- Modules
  - SimplySql
- Many of these functions require PDQ Deploy and/or Inventory to be installed.

# Functions

## Main

These are the primary functions I expect most people will interact with.

Function | Description | Database Access
--- | --- | ---
Compare-PdqInventoryComputers | Compares 2 computers from your Inventory database. | R
Connect-PdqInventoryScanner | Connects Scanners to a Scan Profile by reading the contents of the clipboard. | R/W
Convert-PdqDocumentationLink | Converts documentation.pdq.com links into link.pdq.com links. | -
Enable-PdqPackageAlwaysPrioritized | Allows packages to be tagged so their deployments will always be prioritized. | R/W
Get-PdqConsoleUserSession | Retrieves Console User sessions from the Deploy or Inventory database. | R
Get-PdqCustomField | Retrieves Custom Field data from the Inventory database. | R
Get-PdqDatabaseTable | Retrieves information about database tables. | R
Get-PdqDeployment | Retrieves information about deployments from the Deploy database. | R
Get-PdqInventoryScanErrors | Retrieves scan errors from the PDQ Inventory database. | R
Get-PdqLatestVersion | Asks a PDQ API what the latest version number is. | -
Get-PdqLicense | Retrieves and decodes your PDQ license(s). | -
Get-PdqStuffSetting | Retrieves PdqStuff settings. | -
Get-PdqVariable | Retrieves the value of a Variable from the database of Deploy or Inventory. | R
Hide-PdqNonDownloadedPackageUpdates | Removes entries from the Updates tab for packages you haven't downloaded. | R/W
Invoke-PdqPkgDownload | Downloads .pdqpkg files. | -
Measure-PdqDeploymentStartDelay | Calculates how long it took for each deployment in PDQ Deploy to start. | R
Move-PdqDeployItem | Moves whatever you select into a Folder by reading the contents of the clipboard. | R/W
Remove-PdqStuffSetting | Deletes PdqStuff settings. | -
Set-PdqCustomVariable | Creates or updates a Custom Variable in Deploy or Inventory. | R/W
Set-PdqStuffSetting | Creates or updates PdqStuff settings. | -
Start-PdqDeployment | A wrapper for `PDQDeploy.exe Deploy` that adds extra features. | R
Sync-PdqVariable | Synchronizes variables between Deploy and Inventory. | R/W
Wait-PdqDeployment | Waits for the specified deployments to finish. | R

## Helpers

These are helper functions for the Main functions. I decided to leave them in Public for now in case other people want to use them in their scripts.

Function | Description | Database Access
--- | --- | ---
Assert-PdqMinimumVersion | Compares the database version against a specified version. | R
Close-PdqSqlConnection | Closes a SimplySql connection that Open-PdqSqlConnection opened. | -
Convert-PdqVariableType | Converts a Variable name from one type to another. | -
Get-PdqClipboardData | Retrieves PDQ data from the clipboard. | -
Get-PdqClipboardFormat | Outputs a format name and data type for Get-PdqClipboardData. | -
Get-PdqDatabaseData | Retrieves all rows from the specified table. | R
Get-PdqDatabasePath | Retrieves the path to the database of the specified product and tests it. | R
Get-PdqInstalledVersion | Searches the registry for the currently installed version. | -
Get-PdqInventoryComputerData | Retrieves all rows from the specified table for the specified computer names. | R
Get-PdqPackageLibraryCatalog | Retrieves the Package Library catalog from the PDQ web API. | -
Invoke-PdqSqlQuery | Executes a SQL query against a PDQ database. | R/W
Open-PdqSqlConnection | Creates a SimplySql connection to the database of the specified product. | R
Resolve-PdqInventoryComputerName | Retrieves the ComputerId of the specified computer from the Inventory Database. | R
Select-PdqPackageLibraryPackage | Searches for packages in the Package Library catalog. | -
Test-PdqVariableName | Determines whether a Variable is Custom, System, or "Bare". | -
Wait-PdqClipboardData | Waits for a specific type of data to appear on the clipboard. | -

# Installing

```PowerShell
Install-Module -Name 'PdqStuff'
```

# Settings

PdqStuff looks in the `HKCU:\SOFTWARE\PdqStuff` Registry key for optional settings. These must be manually created, for now.

Example:
```PowerShell
New-Item -Path 'HKCU:\SOFTWARE' -Name 'PdqStuff' -ErrorAction 'SilentlyContinue'
New-ItemProperty -Path 'HKCU:\SOFTWARE\PdqStuff' -Name 'DisableImportWarning' -Value 1 -PropertyType 'DWORD'
```

Name | Type | Expected Values | Description
--- | --- | --- | ---
DisableImportWarning | DWORD | 0, 1 | If this is set to 1, no warning will be displayed when the module is imported.
