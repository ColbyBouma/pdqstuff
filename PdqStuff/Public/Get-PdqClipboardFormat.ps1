<#
.SYNOPSIS
Outputs format names and their data type for Get-PdqClipboardData.

.DESCRIPTION
See Get-PdqClipboardData for an explanation of these format names.

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject
System.Object[]

.EXAMPLE
Get-PdqClipboardFormat
Outputs all available formats.

.EXAMPLE
Get-PdqClipboardFormat -Format 'Package'
Outputs a PSCustomObject containing the format name and data type for Packages.
#>
function Get-PdqClipboardFormat {

    [CmdletBinding()]
    param (
        # The short names of the formats you would like to look up.
        [String[]]$Format
    )

    $FormatTable = [Ordered]@{
        'AutoReport'   = [PSCustomObject]@{
            'Name' = 'x-application/PDQAutoReportDefinitions'
            'Type' = 'JSON'
        }
        'Collection'   = [PSCustomObject]@{
            'Name' = 'AdminArsenal.PDQInventory.MainTree.Export'
            'Type' = 'XML'
        }
        'DeployFolder' = [PSCustomObject]@{
            'Name' = 'AdminArsenal.MainTreeControl.Clipboard'
            'Type' = 'XML'
        }
        'Filter'       = [PSCustomObject]@{
            'Name' = 'AdminArsenal.Reports.Engine.Basic.FilterViewModel'
            'Type' = 'JSON'
        }
        'Notification' = [PSCustomObject]@{
            'Name' = 'x-application/PDQDeployMailNotification'
            'Type' = 'JSON'
        }
        'Package'      = [PSCustomObject]@{
            'Name' = 'AdminArsenal.MainTreeControl.Clipboard'
            'Type' = 'XML'
        }
        'PackageStep'  = [PSCustomObject]@{
            'Name' = 'AdminArsenal.PDQDeploy.PackageStepViewModel'
            'Type' = 'JSON'
        }
        'ReportFolder' = [PSCustomObject]@{
            'Name' = 'AdminArsenal.PDQInventory.MainTree.Export'
            'Type' = 'XML'
        }
        'Scanner'      = [PSCustomObject]@{
            'Name' = 'AdminArsenal.PDQInventory.Scanners.ScannerViewModel'
            'Type' = 'JSON'
        }
        'ScanProfile'  = [PSCustomObject]@{
            'Name' = 'AdminArsenal.PDQInventory.Scanners.ScanProfilesPageViewModel'
            'Type' = 'XML'
        }
        'TargetList'   = [PSCustomObject]@{
            'Name' = 'AdminArsenal.MainTreeControl.Clipboard'
            'Type' = 'XML'
        }
        'Tool'         = [PSCustomObject]@{
            'Name' = 'AdminArsenal.ComputerToolSettings.Clipboard'
            'Type' = 'XML'
        }
        'Trigger'      = [PSCustomObject]@{
            'Name' = 'AdminArsenal.ScheduleTriggers.ScheduleTriggerViewModel'
            'Type' = 'JSON'
        }
        'Variable'     = [PSCustomObject]@{
            'Name' = 'AdminArsenal.Variables.CustomVariableViewModel'
            'Type' = 'JSON'
        }
    }

    if ( $Format ) {

        foreach ( $FormatName in $Format ) {
        
            if ( $FormatName -notin $FormatTable.Keys ) {

                throw "'$FormatName' is not a valid format."

            }
    
            $FormatTable.$FormatName

        }

    } else {

        [PSCustomObject]$FormatTable

    }

}