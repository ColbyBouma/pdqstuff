<#
.SYNOPSIS
Compares 2 computers from your Inventory database.

.DESCRIPTION
This function defaults to showing the Name and Version columns from the Applications table.
You can use the -Table and -Columns parameters to compare whatever data you want.

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject
System.Object[]

.EXAMPLE
Compare-PdqInventoryComputers -ReferenceComputer 'TEMPLATE' -DifferenceComputer 'SALLY-LT' | Sort-Object Name
Shows the difference between the applications that TEMPLATE and SALLY-LT have installed, and sorts by Name to make
it easier to see when both computers have the same application, but different versions.

.EXAMPLE
Compare-PdqInventoryComputers 'TEMPLATE' 'SALLY-LT' -Columns 'Name' | Sort-Object 'SideIndicator', 'Name'
Same as the previous example, but without the Version column. This one is sorted by SideIndicator first to make it
easier to see which computers have which applications.

.EXAMPLE
Compare-PdqInventoryComputers 'TEMPLATE' 'SALLY-LT' -ExcludeDifferent -IncludeEqual | Sort-Object 'Name'
Shows which applications are the same on both computers.

.EXAMPLE
Compare-PdqInventoryComputers -ReferenceComputer 'BOB' -DifferenceComputer 'SAM' -Table 'Printers' -Columns 'Name'
Shows the difference between the printers that BOB and SAM have installed.
#>
function Compare-PdqInventoryComputers {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # The name or hostname of a computer you would like to compare another computer against.
        [String]$ReferenceComputer,

        [Parameter(Mandatory = $true)]
        # The name or hostname of a computer you would like to compare to $ReferenceComputer.
        [String]$DifferenceComputer,

        # The database table you would like to query.
        # Use the following command to see a list of available tables:
        # Get-PdqDatabaseTable -Product 'Inventory' 
        [String]$Table = 'Applications',

        # The columns of the database table you would like to see.
        # Use the Columns property from Get-PdqDatabaseTable to see the available columns for your desired table.
        [String[]]$Columns = @('Name', 'Version'),

        # Don't show differences.
        # This will probably result in nothing being returned unless you use -IncludeEqual as well.
        # See the help for Compare-Object for more information.
        [Switch]$ExcludeDifferent,

        # Include rows that match on both computers.
        # See the help for Compare-Object for more information.
        [Switch]$IncludeEqual,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    try {

        $CloseConnection = Open-PdqSqlConnection -Product 'Inventory' -DatabasePath $DatabasePath

        $ReferenceComputerData = Get-PdqInventoryComputerData -Name $ReferenceComputer -Table $Table -Columns $Columns
        $DifferenceComputerData = Get-PdqInventoryComputerData -Name $DifferenceComputer -Table $Table -Columns $Columns

        $Params = @{
            'ReferenceObject'  = $ReferenceComputerData
            'DifferenceObject' = $DifferenceComputerData
            'Property'         = $Columns
            'ExcludeDifferent' = $ExcludeDifferent
            'IncludeEqual'     = $IncludeEqual
        }
        Compare-Object @Params

    } finally {

        Close-PdqSqlConnection -Product 'Inventory' -CloseConnection $CloseConnection

    }

}