<#
.SYNOPSIS
Converts a Variable name from one type to another.

.INPUTS
None.

.OUTPUTS
System.String

.EXAMPLE
Convert-PdqVariableType -Name 'ServerIp' -Type 'Custom'
@(ServerIp)
#>
function Convert-PdqVariableType {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # The Variable name you would like to convert to a different type.
        # Make sure to use single quotes with System Variables, otherwise PowerShell will throw an error!
        # '$(Date)'
        [String]$Name,

        [Parameter(Mandatory = $true)]
        [ValidateSet('Bare', 'Custom', 'System')]
        # The type of Variable you would like to convert $Name to.
        [String]$Type
    )

    switch ($Type) {
        'Bare' {
            $Name -replace '[$@()]'
        }
        'Custom' {
            "@($Name)"
        }
        'System' {
            "`$($Name)"
        }
    }

}