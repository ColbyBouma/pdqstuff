<#
.SYNOPSIS
Retrieves Console User sessions from the Deploy or Inventory database.

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject
System.Object[]
System.Int32

.EXAMPLE
Get-PdqConsoleUserSession -Product 'Deploy'
Outputs all Console User sessions from Deploy.

.EXAMPLE
Get-PdqConsoleUserSession -Product 'Inventory' -Total
Outputs the total number of Console User sessions from Inventory.
#>
function Get-PdqConsoleUserSession {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('Deploy', 'Inventory')]
        # The PDQ application you would like to execute this function against.
        [String]$Product,

        # A list of usernames you would like to search for.
        [String[]]$UserName,

        # Output the total number of sessions instead of the sessions themselves.
        [Switch]$Total,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    $TotalSessions = 0
    $FoundUserNames = New-Object System.Collections.Generic.List[String]

    Get-PdqDatabaseData -Product $Product -Table 'ConsoleUserSessions' -DatabasePath $DatabasePath | ForEach-Object {

        $ConsoleUserSession = $_

        $FoundUserName = $ConsoleUserSession.UserName
        if ( $FoundUserName.Contains('\') ) {

            $FoundUserName = ($FoundUserName -split '\\')[1]

        }
        $FoundUserNames.Add($FoundUserName)

        if ( (-not $UserName) -or ($FoundUserName -in $UserName) ) {
    
            # Split CSV fields into arrays.
            $ConsoleUserSession.CLI = $ConsoleUserSession.CLI -split ','
            $ConsoleUserSession.Console = $ConsoleUserSession.Console -split ','

            # Concatenate the session arrays, then remove empty entries.
            $AllSessions = $ConsoleUserSession.CLI + $ConsoleUserSession.Console | Where-Object { $_ }
            # Deduplicate the list, then count how many are left.
            $SessionCount = (($AllSessions | Group-Object).Name).Count

            if ( $Total ) {

                $TotalSessions += $SessionCount

            } else {

                # Add Sessions as a property, then output the object.
                $ConsoleUserSession | Add-Member -MemberType 'NoteProperty' -Name 'Sessions' -Value $SessionCount -PassThru

            }

        }
        
    }

    foreach ( $DesiredUserName in $UserName ) {

        if ( $DesiredUserName -notin $FoundUserNames ) {

            throw "No Console User sessions were found for: $DesiredUserName"

        }

    }

    if ( $Total ) {

        $TotalSessions

    }

}