<#
.SYNOPSIS
Searches for packages in the Package Library catalog.

.INPUTS
None.

.OUTPUTS
System.Object[]
System.Xml.XmlElement

.EXAMPLE
Select-PdqPackageLibraryPackage -PackageName 'Google Chrome Enterprise', 'Mozilla Firefox'
Searches for Chrome and Firefox in the catalog, and emits XmlElement objects if they are found.
#>

function Select-PdqPackageLibraryPackage {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # The names of the packages you would like to retrieve from the Package Library catalog.
        [String[]]$PackageName,

        # How long you want to wait before downloading a fresh copy of the Package Library catalog.
        [UInt32]$CacheTimeoutSeconds = 600,

        # The object that Get-PdqPackageLibraryCatalog emits.
        # Omitting this will cause Get-PdqPackageLibraryCatalog to be called.
        [XML]$LibraryCatalog
    )

    if ( -not $LibraryCatalog ) {

        $LibraryCatalog = Get-PdqPackageLibraryCatalog -CacheTimeoutSeconds $CacheTimeoutSeconds

    }

    foreach ( $NameOfPackage in $PackageName ) {
    
        $PackageEntry = $LibraryCatalog.Library.Package | Where-Object 'Name' -eq $NameOfPackage

        if ( $PackageEntry.Name.Count -eq 0 ) {

            throw "Unable to find a match for '$NameOfPackage'. Please use the exact name as seen in the Package Library."

        }

        if ( $PackageEntry.Name.Count -gt 1 ) {

            throw "Found too many matches for '$NameOfPackage'. Please use the exact name as seen in the Package Library."

        }

        $PackageEntry

    }

}