<#
.SYNOPSIS
Determines whether a Variable is Custom, System, or "Bare".

.NOTES
"Bare" is my term for a Variable name that doesn't have the Custom or System formatting characters.

.INPUTS
None.

.OUTPUTS
System.String

.EXAMPLE
Test-PdqVariableName -Name 'Date'
Bare

.EXAMPLE
Test-PdqVariableName -Name '@(Date)'
Custom

.EXAMPLE
Test-PdqVariableName -Name '$(Date)'
System
#>
function Test-PdqVariableName {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # The Variable name you would like to examine.
        [String]$Name
    )

    switch ($Name) {
        # Starts with @(
        # Contains 1 or more of any character, except for these: ()@$
        # Ends with )
        {$_ -match '^@\([^()@$]+\)$'} {
            'Custom'
            break
        }
        # Starts with $(
        # Contains 1 or more of any character, except for these: ()@$
        # Ends with )
        {$_ -match '^\$\([^()@$]+\)$'} {
            'System'
            break
        }
        # Contains 1 or more of any character, except for these: ()@$
        {$_ -match '^[^()@$]+$'} {
            'Bare'
            break
        }
        default {
            throw "'$_' does not match the Variable naming convention."
        }
    }
    
}