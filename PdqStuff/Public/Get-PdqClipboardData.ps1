<#
.SYNOPSIS
Retrieves PDQ data from the clipboard.

.DESCRIPTION
When you copy items in PDQ (Packages, Collections, etc.), they exist in a special section of the clipboard.
To access these special sections, you have to call a .NET method with their format name.
I created Get-PdqClipboardFormat to translate easy names (Package, Collection, etc.) into these format names.
After these special sections are accessed, the data they contain (XML, JSON) must be converted.

.INPUTS
None.

.OUTPUTS
System.Xml.XmlDocument
System.Management.Automation.PSCustomObject

.EXAMPLE
Get-PdqClipboardData -Format 'Package'
Retrieves a Package from the clipboard and outputs the converted data.
#>
function Get-PdqClipboardData {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # Run Get-PdqClipboardFormat to get a list of formats.
        [String]$Format
    )

    $FormatData = Get-PdqClipboardFormat -Format $Format

    # Retrieve the PDQ data from the clipboard.
    $ClipboardData = [System.Windows.Forms.Clipboard]::GetData($FormatData.Name)
    
    # Convert the clipboard data to a PowerShell object.
    switch ( $FormatData.Type ) {
        'JSON' {
            $ClipboardData | ConvertFrom-Json
            break
        }
        'XML' {
            [XML]$ClipboardData
            break
        }
    }

}