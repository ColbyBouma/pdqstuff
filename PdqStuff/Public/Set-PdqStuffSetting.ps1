<#
.SYNOPSIS
Creates or updates PdqStuff settings.

.NOTES
https://gitlab.com/ColbyBouma/pdqstuff#settings

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject

.EXAMPLE
Set-PdqStuffSetting -Name 'DisableImportWarning' -Value 1
Disables the warning that is shown when importing this module.

Set-PdqStuffSetting -Name 'DisableImportWarning'
Resets 'DisableImportWarning' to its default value.
#>
function Set-PdqStuffSetting {

    [CmdletBinding()]
    param (
        # The name of the setting you would like to adjust. See the README for a list of available settings:
        # https://gitlab.com/ColbyBouma/pdqstuff#settings
        [Parameter(Mandatory = $true)]
        [String]$Name,

        # The value you would like to change the setting to.
        $Value
    )

    $AvailableSettings = @{
        'DisableImportWarning' = @{
            'AcceptedValues' = 0, 1
            'DefaultValue'   = 0
            'Type'           = 'DWORD'
        }
    }

    if ( $Name -notin $AvailableSettings.Keys ) {

        throw 'Unknown setting name.'

    }

    if ( -not $Value ) {

        $Value = $AvailableSettings[$Name].DefaultValue

    }

    if ( $Value -notin $AvailableSettings[$Name].AcceptedValues ) {

        throw 'The value specified does not match the list of accepted values.'

    }

    if ( -not (Test-Path -Path 'HKCU:\SOFTWARE\PdqStuff') ) {

        $null = New-Item -Path 'HKCU:\SOFTWARE' -Name 'PdqStuff' -ErrorAction 'Stop'
        Write-Verbose 'Created HKCU:\SOFTWARE\PdqStuff'

    }

    try {
    
        $ExistingValue = Get-ItemPropertyValue -Path 'HKCU:\SOFTWARE\PdqStuff' -Name $Name -ErrorAction 'Stop'
        $null = Set-ItemProperty -Path 'HKCU:\SOFTWARE\PdqStuff' -Name $Name -Value $Value

    } catch {

        $Parameters = @{
            'Path'         = 'HKCU:\SOFTWARE\PdqStuff'
            'Name'         = $Name
            'Value'        = $Value
            'PropertyType' = $AvailableSettings[$Name].Type
        }
        $null = New-ItemProperty @Parameters

    }

    [PSCustomObject]@{
        'Name'     = $Name
        'OldValue' = $ExistingValue
        'NewValue' = $Value
    }

}