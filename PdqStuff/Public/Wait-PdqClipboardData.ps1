<#
.SYNOPSIS
Waits for a specific type of data to appear on the clipboard.

.DESCRIPTION
Continuously queries the clipboard to see if it contains any of the specified formats.
If the timeout is exceeded, an error will be returned.

.INPUTS
None.

.OUTPUTS
None.
System.String

.EXAMPLE
Wait-PdqClipboardData -Format 'Collection'
Waits for a PDQ Inventory Collection to appear on the clipboard.
#>
function Wait-PdqClipboardData {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # Run Get-PdqClipboardFormat to get a list of formats.
        [String[]]$Format,

        # The maximum number of seconds you are willing to wait.
        [UInt32]$Timeout = 300
    )

    # Clear the clipboard in case there is already PDQ data on it.
    [System.Windows.Forms.Clipboard]::Clear()
    
    # Wait for the clipboard to contain data in the desired format.
    $FoundData = $false
    $StopWatch = [System.Diagnostics.Stopwatch]::StartNew()
    While (-not $FoundData) {

        foreach ( $FormatIterator in $Format ) {

            $FormatData = Get-PdqClipboardFormat -Format $FormatIterator

            if ( [System.Windows.Forms.Clipboard]::ContainsData($FormatData.Name) ) {

                $FoundData = $true

                # Output which format was found if multiple formats were specified.
                if ( $Format.Count -gt 1 ) {

                    $FormatIterator

                }

            }

        }

        Start-Sleep -Milliseconds 250

        if ( $StopWatch.Elapsed.TotalSeconds -ge $Timeout ) {

            $StopWatch.Stop()
            throw 'Exceeded timeout for Wait-PdqClipboard.'

        }

    }
    $StopWatch.Stop()

}