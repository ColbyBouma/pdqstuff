<#
.SYNOPSIS
Waits for the specified deployments to finish.

.INPUTS
None.

.OUTPUTS
None.

.EXAMPLE
Wait-PdqDeployment -Id 42, 43
Waits until both deployments have finished.
#>

function Wait-PdqDeployment {

    [CmdletBinding()]
    param (
        # The IDs of the deployments you would like to wait for.
        [UInt32[]]$Id,

        # The number of milliseconds you would like to wait for between database queries.
        [Int32]$SleepMilliseconds = 1000,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    try {

        $CloseConnection = Open-PdqSqlConnection -Product 'Deploy' -DatabasePath $DatabasePath
        $WaitCount = 0

        do {

            $Deployments = Get-PdqDeployment -Id $Id -DatabasePath $DatabasePath | Where-Object 'Status' -ne 'Finished'
            'Waiting for {0} deployment(s) to finish.' -f $Deployments.Count | Write-Verbose

            # Prune the list of deployments to check on the next loop.
            $Id = $Deployments.DeploymentId

            if ( $Deployments ) {

                foreach ( $Deployment in $Deployments ) {

                    'ID: {0}, Status: {1}' -f $Deployment.DeploymentId, $Deployment.Status | Write-Verbose

                }

                Start-Sleep -Milliseconds $SleepMilliseconds
                $WaitCount ++

            }


        } until ( -not $Deployments )

    } finally {

        Close-PdqSqlConnection -Product 'Deploy' -CloseConnection $CloseConnection

        if ( $WaitCount -eq 0 ) {

            Write-Warning 'Never waited, something probably went wrong.'

        } else {

            Write-Verbose "Waited $WaitCount times."

        }

    }

}