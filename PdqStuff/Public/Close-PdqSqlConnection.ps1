<#
.SYNOPSIS
Closes a SimplySql connection that Open-PdqSqlConnection opened.

.INPUTS
None.

.OUTPUTS
None.

.EXAMPLE
Close-PdqSqlConnection -Product 'Inventory'
#>
function Close-PdqSqlConnection {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('Deploy', 'Inventory')]
        # The PDQ application you would like to execute this function against.
        [String]$Product,

        # Whether or not the connection should be closed.
        # This is to reduce the number of lines of code in functions that rely on this function.
        [Bool]$CloseConnection
    )

    $CallingFunction = (Get-PSCallStack)[1].Command

    if ( $CloseConnection ) {

        Close-SqlConnection -ConnectionName $Product
        Write-Verbose "$CallingFunction closed the $Product connection."

    } else {

        Write-Verbose "$CallingFunction will not close the connection for $Product."

    }
    
}