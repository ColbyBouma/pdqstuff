<#
.SYNOPSIS
Converts documentation.pdq.com links into link.pdq.com links.

.DESCRIPTION
The main reason you would want to convert links is because link.pdq.com automatically redirects
to the current product version. documentation.pdq.com links are hard-coded to a specific version.

.INPUTS
None.

.OUTPUTS
System.String

.EXAMPLE
Convert-PdqDocumentationLink
Retrieves the documentation.pdq.com link from your clipboard and outputs the link.pdq.com link.

.EXAMPLE
Convert-PdqDocumentationLink -Uri 'https://documentation.pdq.com/PDQDeploy/19.3.36.0/welcome-to-pdq-deploy.htm'
You can specify a link if you don't want this function to read your clipboard.

.EXAMPLE
Convert-PdqDocumentationLink -CopyToClipboard
Same as example #1, but also copies the link.pdq.com link to your clipboard.
#>
function Convert-PdqDocumentationLink {

    [CmdletBinding()]
    param (
        # The documentation.pdq.com link you wish to convert.
        [Uri]$Uri = (Get-Clipboard),

        # Copy the link.pdq.com link to your clipboard.
        [Switch]$CopyToClipboard
    )

    if ( $Uri.Host -ne 'documentation.pdq.com' ) {

        throw 'This script only works with documentation.pdq.com links.'

    }

    $FinalUri = 'https://link.pdq.com/docs-'

    # PDQDeploy or PDQInventory.
    $FinalUri += $Uri.Segments[1] -replace '/', '?'

    # The page, such as auto-reports.htm.
    if ( $Uri.Segments[3] -eq 'index.html' ) {

        $FinalUri += $Uri.Query -replace '\?'

    } else {

        $FinalUri += $Uri.Segments[3]

    }

    # The anchor, such as #mail.
    $FinalUri += $Uri.Fragment

    $FinalUri

    if ( $CopyToClipboard ) {

        $FinalUri | Set-Clipboard
        Write-Verbose 'Copied to clipboard.'

    }

}