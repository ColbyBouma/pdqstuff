<#
.SYNOPSIS
Retrieves all rows from the specified table.

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject
System.Object[]

.EXAMPLE
Get-PdqDatabaseData -Product 'Deploy' -Table 'DatabaseVersionHistory'
Outputs all rows from Deploy's DatabaseVersionHistory table.
#>
function Get-PdqDatabaseData {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('Deploy', 'Inventory')]
        # The PDQ application you would like to execute this function against.
        [String]$Product,

        [Parameter(Mandatory = $true)]
        [ArgumentCompleter( {
                param ($CommandName, $ParameterName, $WordToComplete, $CommandAst, $FakeBoundParameters)

                if ( $FakeBoundParameters.ContainsKey('Product') ) {
                
                    $Params = @{
                        'Product' = $FakeBoundParameters.Product
                    }
            
                    if ( $FakeBoundParameters.ContainsKey('DatabasePath') ) {

                        $Params += @{
                            'DatabasePath' = $FakeBoundParameters.DatabasePath
                        }

                    }

                    (Get-PdqDatabaseTable @Params).Name | Where-Object { $_ -like "$WordToComplete*" }

                }
            })]
        # The database table you would like to query.
        # This parameter supports tab-completion.
        # Make sure to specify -Product first, and -DatabasePath (only if you need it).
        # Hit CTRL+SPACE to see all tables, or type a few characters and hit TAB.
        [String]$Table,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    try {

        $CloseConnection = Open-PdqSqlConnection -Product $Product -DatabasePath $DatabasePath

        # Verify the table name.
        $null = Get-PdqDatabaseTable -Name $Table -Product $Product -DatabasePath $DatabasePath
    
        Invoke-PdqSqlQuery -Query "SELECT * FROM $Table;" -Product $Product -DatabasePath $DatabasePath -Stream

    } finally {

        Close-PdqSqlConnection -Product $Product -CloseConnection $CloseConnection

    }

}