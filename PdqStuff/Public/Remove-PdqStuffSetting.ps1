<#
.SYNOPSIS
Deletes PdqStuff settings.

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject
System.Object[]

.EXAMPLE
Remove-PdqStuffSetting -Name 'DisableImportWarning'
Deletes the 'DisableImportWarning' setting.

.EXAMPLE
Remove-PdqStuffSetting -All
Deletes all PdqStuff settings.
#>
function Remove-PdqStuffSetting {

    [CmdletBinding()]
    param (
        # The names of the settings you would like to delete.
        [Parameter(ParameterSetName = 'Name', Mandatory = $true)]
        [String[]]$Name,

        # Delete all settings.
        [Parameter(ParameterSetName = 'All', Mandatory = $true)]
        [Switch]$All
    )

    if ( $All ) {

        Get-PdqStuffSetting
        Remove-Item -Path 'HKCU:\SOFTWARE\PdqStuff' -ErrorAction 'SilentlyContinue'

    } else {

        Get-PdqStuffSetting -Name $Name
        Remove-ItemProperty -Path 'HKCU:\SOFTWARE\PdqStuff' -Name $Name -ErrorAction 'SilentlyContinue'

    }

}