<#
.SYNOPSIS
Retrieves PdqStuff settings.

.NOTES
This will return nothing if you have not set any settings.

.INPUTS
None.

.OUTPUTS
System.Management.Automation.PSCustomObject
System.Object[]

.EXAMPLE
Get-PdqStuffSetting
Outputs all PdqStuff settings.

.EXAMPLE
Get-PdqStuffSetting -Name 'DisableImportWarning'
Outputs the value of the 'DisableImportWarning' setting.
#>
function Get-PdqStuffSetting {

    [CmdletBinding()]
    param (
        # A list of setting names you would like to retrieve.
        [String[]]$Name
    )

    $AllValues = Get-ItemProperty -Path 'HKCU:\SOFTWARE\PdqStuff' -ErrorAction 'SilentlyContinue'

    foreach ( $Setting in ((Get-Item -Path 'HKCU:\SOFTWARE\PdqStuff' -ErrorAction 'SilentlyContinue').Property) ) {

        if ( $Name ) {

            if ( $Setting -notin $Name ) {

                continue

            }

        }
        
        [PSCustomObject]@{
            'Name'  = $Setting
            'Value' = $AllValues.$Setting
        }

    }

}