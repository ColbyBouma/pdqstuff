<#
.SYNOPSIS
Compares the database version against a specified version.

.DESCRIPTION
Verifies the database version is equal to or greater than the specified minimum version.
If the database version is lower than the minimum version, this function will throw an error.

.INPUTS
None.

.OUTPUTS
None.

.EXAMPLE
Assert-PdqMinimumVersion -Product 'Deploy' -MinimumVersion '19.3.36.0'
Verifies that the version in PDQ Deploy's database is at least 19.3.36.0.
#>
function Assert-PdqMinimumVersion {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('Deploy', 'Inventory')]
        # The PDQ application you would like to execute this function against.
        [String]$Product,

        [Parameter(Mandatory = $true)]
        # The version number you want to compare against the database version.
        [Version]$MinimumVersion,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    $Query = 'SELECT Version FROM DatabaseInfo;'
    [Version]$DatabaseVersion = Invoke-PdqSqlQuery -Product $Product -DatabasePath $DatabasePath -Query $Query -QueryType 'Scalar'

    if ( $DatabaseVersion -lt $MinimumVersion ) {

        $CallingFunction = (Get-PSCallStack)[1].Command
        throw "$CallingFunction requires a minimum PDQ $Product database version of $MinimumVersion. You have $DatabaseVersion."

    }

}