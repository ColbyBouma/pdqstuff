<#
.SYNOPSIS
Retrieves the ComputerId of the specified computer from the Inventory Database.

.INPUTS
None.

.OUTPUTS
System.Int32

.EXAMPLE
Resolve-PdqInventoryComputerName -Name 'FRED5'
#>
function Resolve-PdqInventoryComputerName {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        # The name or hostname of the computer you would like to look up.
        [String]$Name,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    try {

        $CloseConnection = Open-PdqSqlConnection -Product 'Inventory' -DatabasePath $DatabasePath

        foreach ( $NameType in 'Name', 'HostName' ) {

            $ComputerIdQuery = "SELECT ComputerId FROM Computers WHERE $NameType = '$Name';"
            $ComputerId = Invoke-SqlScalar -Query $ComputerIdQuery -ConnectionName 'Inventory'

            # Exit the loop if a match is found.
            if ( $ComputerId ) {

                break

            }

        }

        if ( -not $ComputerId ) {

            throw "Unable to find a ComputerId for: $Name"

        }

        [Int32]$ComputerId

    } finally {

        Close-PdqSqlConnection -Product 'Inventory' -CloseConnection $CloseConnection

    }

}