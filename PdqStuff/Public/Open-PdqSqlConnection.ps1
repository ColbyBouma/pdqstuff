<#
.SYNOPSIS
Creates a SimplySql connection to the database of the specified product.

.INPUTS
None.

.OUTPUTS
System.Boolean

.EXAMPLE
$CloseConnection = Open-PdqSqlConnection -Product 'Inventory'
#>
function Open-PdqSqlConnection {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]    
        [ValidateSet('Deploy', 'Inventory')]
        # The PDQ application you would like to execute this function against.
        [String]$Product,

        # The path to the currently active database will be retrieved by default.
        # You can use this parameter if you wish to run this function against a different database.
        [String]$DatabasePath
    )

    $CallingFunction = (Get-PSCallStack)[1].Command

    # Reuse the existing connection.
    if ( (Show-SqlConnection -All) -contains $Product) {
        
        $CloseConnection = $false
        Write-Verbose "$CallingFunction will reuse the $Product connection."
        
    } else {
        
        $CloseConnection = $true
        Write-Verbose "$CallingFunction is opening a connection for $Product."
        $DatabasePath = Get-PdqDatabasePath -Product $Product -DatabasePath $DatabasePath
        Open-SQLiteConnection -DataSource $DatabasePath -ConnectionName $Product

    }

    $CloseConnection
    
}