<#
.SYNOPSIS
Searches the registry for the currently installed version.

.DESCRIPTION
Installed applications have entries in the following registry keys:
    32-bit app on 32-bit OS, or 64-bit app on 64-bit OS:
        HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall
    32-bit app on 64-bit OS:
        HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall
PDQ Deploy and Inventory are .NET Framework applications that use the AnyCPU architecture,
so the architecture of the OS determines which registry path their entries will exist in.

If a matching registry entry cannot be found, this function will return an error.

.INPUTS
None.

.OUTPUTS
System.Version

.EXAMPLE
Get-PdqInstalledVersion -Product 'Deploy'
Outputs the version number for the currently installed instance of PDQ Deploy.
#>
function Get-PdqInstalledVersion {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [ValidateSet('Deploy', 'Inventory')]
        # The PDQ application you would like to execute this function against.
        [String]$Product
    )

    # Determine which registry path should be searched.
    $Software = 'SOFTWARE'
    if ( [Environment]::Is64BitOperatingSystem ) {

        $Software += '\WOW6432Node'

    }
    $RegPath = "HKLM:\$Software\Microsoft\Windows\CurrentVersion\Uninstall\*"

    # Search the registry for information about installed applications.
    $RegData = Get-ItemProperty -Path $RegPath | Where-Object 'DisplayName' -eq "PDQ $Product"
    if ( $RegData ) {

        # A single result will be a PSCustomObject, but multiple results will show up as Object[].
        if ( $RegData -is 'PSCustomObject' ) {
                
            [Version]$Version = $RegData.DisplayVersion
            Write-Verbose "Installed PDQ $Product version: $Version"
            $Version

        } else {

            throw "Found multiple registry entries with a DisplayName of: PDQ $Product"

        }

    } else {

        throw "Unable to find a version number for PDQ $Product. Please make sure it is installed."

    }

}