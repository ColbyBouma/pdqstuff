if ( (Get-ItemProperty -Path 'HKCU:\SOFTWARE\PdqStuff' -ErrorAction 'SilentlyContinue').DisableImportWarning -ne 1 ) {

    Write-Warning 'Many of these functions access your PDQ databases. Please back them up before proceeding.'

}

# This is required for the clipboard functions.
Add-Type -AssemblyName 'System.Windows.Forms'

# Import all functions.
Get-ChildItem -Path "$PSScriptRoot\Public\*.ps1" | ForEach-Object {

    . $_.FullName

}